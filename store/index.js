import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);
export default new Vuex.Store({
	state:{
		isShowLeftMenu:false,		//是否显示左侧菜单
		innerAudioContext:null,		//音乐播放控制器
		isPlay:false,				//音乐是否播放
		playAnimationData:{},		//播放音乐旋转动画数据
		playAnimationId:""			//播放音乐旋转动画定时器Id
		
	},
	mutations:{
		showLeftMenu(state){
			state.isShowLeftMenu=true;
		},
		hideLeftMenu(state){
			state.isShowLeftMenu=false;
		},
		setInnerAudioContext(state,obj){
			state.innerAudioContext=obj;
		},
		//设置播放
		setPlay(state){
			state.isPlay=false;				  //全局状态标记为暂停
			state.innerAudioContext.pause();  //音乐暂停
			clearInterval(state.playAnimationId);  //播放动画暂停
			state.playAnimationData={};				//清空播放音乐的动画数据
			
			state.innerAudioContext.play();
			
		},
		//设置暂停
		setPause(state){
			state.isPlay=false;				  //全局状态标记为暂停
			state.innerAudioContext.pause();  //音乐暂停
			clearInterval(state.playAnimationId);  //播放动画暂停
			state.playAnimationData={};				//清空播放音乐的动画数据
		},
		//音乐开始播放
		startPlay(state){
			
			state.isPlay=true;
			var animation=uni.createAnimation({
				duration:10000,
				timingFunction:"linear",
			});
			let n=1;
			animation.rotateZ(180*n).step();
			state.playAnimationData=animation.export();
			state.playAnimationId=setInterval(()=>{
				n++;
				animation.rotateZ(180*n).step();
				state.playAnimationData=animation.export();
			},10000);
		}
	},
	actions:{
		showLeftMenu({commit}){
			commit("showLeftMenu");
		},
		hideLeftMenu({commit}){
			commit("hideLeftMenu");
		},
		setInnerAudioContext({commit},obj){
			commit("setInnerAudioContext",obj);
		},
		//设置播放
		setPlay({commit}){
			commit("setPlay");
		},
		//设置暂停
		setPause({commit}){
			commit("setPause");
		},
		//音乐开始播放
		startPlay({commit}){
			commit("startPlay")
		}
	}
});